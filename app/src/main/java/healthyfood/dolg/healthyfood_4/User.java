package healthyfood.dolg.healthyfood_4;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dolg on 22.12.17.
 */

public class User {
    private String name=Constant.DEFAULT_NAME;
    private String email=Constant.DEFAULT_EMAIL;
    private boolean editUser=false;
    private boolean editFood=false;
    private boolean record=false; //есть запись в базу данных
    private String editCity=Constant.Nothing;//город в котором можно добавлять продукты в корзину

    User(){
    }

    User(String mname, String memail, boolean muser, boolean mfood, boolean mrecord, String mcity){
        this.name = mname;
        this.email = memail;
        this.editUser = muser;
        this.editFood = mfood;
        this.record = mrecord;
        this.editCity = mcity;
    }

    public void setUser(String _name, String _email){
        name = _name;
        email = _email;
        Log.d(Constant.MY_TAG,"setUser name:" + name + " email:" + email);
    }

    public void setUser(String _name, String _email, boolean _edit_food, boolean _edit_user,
             boolean _have_record, String _edit_city){
        name = _name;
        email = _email;
        editFood = _edit_food;
        editUser = _edit_user;
        record = _have_record;
        editCity = _edit_city;
    }

    public boolean getEditFood(){
        return editFood;
    }

    public boolean getEditUser(){
        return editUser;
    }

    public boolean getRecord(){
        return record;
    }

    public void setRecordState(boolean _record_state){
        record = _record_state;
    }

    public String getEditCity(){
        return editCity;
    }

    public String getName(){
        return name;
    }

    User(String _name, String _email){
        name = _name;
        email = _email;
    }

    public String getEmail(){
        return email;
    }

    public Bitmap getBitmapFromURL(Uri src) {//нужно делать в соседнем потоке
        try {
            URL url = new URL(src.toString());
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String String(){
         String tmp = "User: name-" + name + ", email-" + email + ", edit_user-" + String.valueOf(editUser)
                + ", edit_food-"+ String.valueOf(editFood)+ ", have_record-"+ String.valueOf(record)
                + ", city_edit-" + editCity;

        return tmp;
    }

    //сохраняем в памяти один город на позиции key
    public void saveUser( Context myContext) {
        SharedPreferences sPref = myContext.getSharedPreferences("name", 0);
        // Извлеките редактор, чтобы изменить Общие настройки.
        SharedPreferences.Editor editor = sPref.edit();
        // Запишите новые значения примитивных типов в объект Общих настроек.
        editor.putString(Constant.PREF_USER, String());
        Log.d(Constant.MY_TAG,"Save user " + Constant.PREF_USER +  String());
        editor.commit();
    }

    //загружаем из памяти один из городов
    public String loadUser(Context myContext) {
        SharedPreferences sPref = myContext.getSharedPreferences("name", 0);
        String savedText = sPref.getString(Constant.PREF_USER, Constant.Nothing);
        Log.d(Constant.MY_TAG,"Read " + Constant.PREF_USER + savedText);
        //разбираем прочитанную строку, и загружаем переменные
        if(savedText.equals(Constant.Nothing)) {
            //ничего не сохранено
        }else{
            //если что то сохранено то разбираем
            getDataUser(savedText);
        }
        return savedText;
    }


    private void getDataUser(String _str){
        String tmp;
        //имя пользователя
        this.name = setVarStr(_str,Constant.USER_NAME);
        tmp=cutStr(_str,this.name);
        //электронная почта
        this.email = setVarStr(tmp,Constant.USER_EMAIL);
        tmp=cutStr(tmp,this.email);
        //user rule
        this.editUser = setVarBool(tmp,Constant.USER_EDIT_USER);
        tmp=cutStr(tmp,String.valueOf(this.editUser));
        //food rule
        this.editFood = setVarBool(tmp,Constant.USER_EDIT_FOOD);
        tmp=cutStr(tmp,String.valueOf(this.editFood));
        //have record
        this.record = setVarBool(tmp,Constant.USER_EDIT_RECORD);
        tmp=cutStr(tmp,String.valueOf(this.record));
        //city that have have rule to change food
        this.editCity = setVarStr(tmp,Constant.USER_EDIT_CITY);
    }

    String setVarStr(String _data, String _exam){
        int begin=0, end=0;
        String m_tmp="";
        begin = _data.indexOf(_exam)+_exam.length();
        end = _data.indexOf(",");
        if(end<0){end =_data.length();}
        try{
            m_tmp = _data.substring(begin,end);
            Log.d(Constant.MY_TAG,"string from preferance: "+ m_tmp);
        }catch(Exception e){
            Log.d(Constant.MY_TAG,"Error load string from preferance: "+ e.getMessage());
        }
        return m_tmp;
    }

    Boolean setVarBool(String _data, String _exam){
        int begin=0, end=0;
        String m_tmp="";
        boolean tmp=false;
        begin = _data.indexOf(_exam)+_exam.length();
        end = _data.indexOf(",");
        try{
            m_tmp = _data.substring(begin,end);
            Log.d(Constant.MY_TAG,"string from preferance: "+ m_tmp);
        }catch(Exception e){
            Log.d(Constant.MY_TAG,"Error load string from preferance: "+ e.getMessage());
        }
        if(!m_tmp.isEmpty()){
            if(m_tmp.equals("true")){
                tmp = true;
            }else {
                tmp = false;
            }
        }else{
            tmp =false;
        }
        return tmp;
    }

    String cutStr(String _data,String _exam) {
        String tmp="";
        int begin=_data.indexOf(_exam)+_exam.length()+1;//+1 чтобы удалить запятую
        try {
            tmp = _data.substring(begin);
        }catch(Exception e){
            Log.d(Constant.MY_TAG,"Error cut string from preferance: "+ e.getMessage());
        }
        Log.d(Constant.MY_TAG,"After cut string from : "+ tmp);
        return tmp;
    }

}
