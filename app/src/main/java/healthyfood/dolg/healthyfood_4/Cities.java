package healthyfood.dolg.healthyfood_4;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Гоша on 19.01.2018.
 */

public class Cities extends AppCompatActivity{
    private static String Name = Constant.CITY_DEFAULT_NAME;
    private final SharedPreferences sPref;
    private final Context myContext;
    private Activity activity;
    private FusedLocationProviderClient mFusedLocationClient;
    public static Location c_location =null;
    private DialogInterface.OnClickListener myOnlickListener;
    private Handler myHandler;
    private boolean getLocation = true;

    Cities(Context _context, Activity _activity){
        myContext = _context;
        sPref = myContext.getSharedPreferences("name", 0);
        activity = _activity;
        getLocation = getAutoGetLocation();
    }

    Cities(Context _context, Activity _activity, Handler _Handler){
        myContext = _context;
        sPref = myContext.getSharedPreferences("name", 0);
        activity = _activity;
        myHandler = _Handler;
        getLocation = getAutoGetLocation();
    }

    public void saveAutoGetLocation(boolean _value){
        SharedPreferences.Editor editor = sPref.edit();
        editor.putBoolean(Constant.PREF_AUTOGET_LOC, _value);
        Log.d(Constant.MY_TAG,"saveAutoGetLocation " + String.valueOf(_value));
        editor.commit();
    }

    public boolean getAutoGetLocation(){
        boolean autoGet = sPref.getBoolean(Constant.PREF_AUTOGET_LOC, true);
        return autoGet;
    }

    public void setName(String _name){
        Name = _name;
        rewriteArray(_name);
        Name = _name;
        Log.d(Constant.MY_TAG,"Set citie's name: " + _name);
    }

    public String getCity(){
        Name = loadCity(0);
        if(Name.equals(Constant.Nothing)&(Name!=null)){
            Name = Constant.CITY_DEFAULT_NAME;
            initArrCity();
        }
        Log.d(Constant.MY_TAG,"Get citie's name: " + Name);
        return Name;
    }

    public static String getCityName() {
        return  Name;
    }

    public static void setLocation(Location _location){
        c_location = _location;
    }

    public static Location getLocation(){
        return c_location;
    }

    //сохраняем в памяти один город на позиции key
    public void saveCity(String str, int key) {
        // Извлеките редактор, чтобы изменить Общие настройки.
        SharedPreferences.Editor editor = sPref.edit();
        // Запишите новые значения примитивных типов в объект Общих настроек.
        editor.putString(Constant.PREF_NAME+String.valueOf(key), str);
        Log.d(Constant.MY_TAG,"Write " + Constant.PREF_NAME+String.valueOf(key) + str);
        editor.commit();
    }

    //загружаем из памяти один из городов
    public String loadCity(int key) {
        String savedText = sPref.getString(Constant.PREF_NAME+String.valueOf(key), Constant.Nothing);
        Log.d(Constant.MY_TAG,"Read " + Constant.PREF_NAME+String.valueOf(key) + savedText);
        return savedText;
    }

    //переписываем массив городов так чтобы выбранный город оказался в начале
    private void rewriteArray(String _name){
        Log.d(Constant.MY_TAG,"Rewrite Array name: " + _name);
        //если город уже на первом месте то ничего не делаем
         if(_name.equals(loadCity(0))){return;}
        String tmp_array[] = new String[Constant.CITY_ARRAY_SIZE];
         //обнуляем массив

        tmp_array[0] = _name;
        int count_el = (Constant.CITY_ARRAY_SIZE-1);//это весь старый массив
        int new_count = 1;//первый элемент занят
        for(int i=0;i<count_el;i++){
            if(_name.equals(loadCity(i))){
                //если такой элемент уже есть в массиве то затираем его
                count_el = count_el+1;
            }else {
                //если такого элемента нет
                tmp_array[(new_count)] = loadCity(i);
                new_count = new_count +1;
            }
            //Log.d(Constant.MY_TAG,"Array rewrite: " + String.valueOf(i) + tmp_array[(i + 1)]);
        }
        for(int j=0;j<(Constant.CITY_ARRAY_SIZE);j++){
            saveCity(tmp_array[j],j);
        }
    }

    public void initArrCity(){
        //Log.d(Constant.MY_TAG,"City 0:" + Array_Cities.loadCity(0));
        if(loadCity(0).equals(Constant.Nothing)){
            //инициализируем массив с названием городов
            Log.d(Constant.MY_TAG,"Init cities array");
            String[] aCities =  myContext.getResources().getStringArray(R.array.cities);
            for(int i=0;i<aCities.length;i++){
                saveCity(aCities[i],i);
            }
        }else{

        }
    }

    public void getPermission(){
        Log.d(Constant.MY_TAG, "Init permission");
        if ((ActivityCompat.checkSelfPermission(myContext, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)) {
            Log.d(Constant.MY_TAG, "Answer permission");
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            Log.d(Constant.MY_TAG, "getPermission Have permission");
        }
    }


    public void getLocated(){
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        if ((ActivityCompat.checkSelfPermission(myContext, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                Log.d(Constant.MY_TAG, "Location is: lat-" + location.getLatitude()
                                        + ", lon-" + location.getLongitude());
                                c_location =  location;
                            }
                        }
                    });
        }else{
            //если не дали разрешения
            Log.d(Constant.MY_TAG, "Havn't permission");
        }
    }


    String getCity_Name(Location __location){
        Geocoder gcd= new Geocoder(activity,Locale.getDefault());
        try {
            List<Address> addresses = gcd.getFromLocation(__location.getLatitude(), __location.getLongitude(), 1);
            if(addresses.size()>0){
                String locality =addresses.get(0).getLocality();
                Log.d(Constant.MY_TAG,"getCityName_4 Result: "+locality);
                if(locality==null) return Constant.Nothing;
                return  locality;
            }
        }catch(IOException e){
            Log.d(Constant.MY_TAG,"getCity_Name Error: " + e.getMessage());
        }catch(NullPointerException ne){
            Log.d(Constant.MY_TAG,"getCityName_4 NullPointer: "+ne.getMessage());
        }
        return Constant.Nothing;
    }

    public String initCity(final String _nameCity) {//переделать
        if (getLocation==false) return"";//если пользователь не хочет автоматически определять город
        //запрашиваем сохраненный город
        String shareCityName = getCity();
        Log.d(Constant.MY_TAG, "initCity _nameCity: " + _nameCity + ", shareCityName: " + shareCityName);
        if(_nameCity == null) return shareCityName;
        if(shareCityName.equals(_nameCity)){
            //совпадают
            Log.d(Constant.MY_TAG,"initCity name the same");
        }else {
            //если загруженное название города не совпадает с данными новигатора
            Log.d(Constant.MY_TAG,"initCity different citie's name");

            myOnlickListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch(which){
                        case Dialog.BUTTON_POSITIVE:
                            Log.d(Constant.MY_TAG,"Dialog click positive");//нейтральный
                            getLocation = false;
                            saveAutoGetLocation(getLocation);
                            break;
                        case Dialog.BUTTON_NEGATIVE:
                            Log.d(Constant.MY_TAG,"Dialog click negative");//нет
                            break;
                        case Dialog.BUTTON_NEUTRAL:
                            Log.d(Constant.MY_TAG,"Dialog click neutral");//да
                            setName(_nameCity);
                            myHandler.sendEmptyMessage(Constant.HANDLER_UPDATE_EXPN);
                            getLocation = true;
                            saveAutoGetLocation(getLocation);
                            break;
                        default:
                            break;
                    }
                }
            };

            AlertDialog.Builder adb = new AlertDialog.Builder(myContext,R.style.AlertDialogStyle);
            adb.setTitle(_nameCity);
            //String str_tmp = myContext.getResources().getString(R.string.change_city_to) +_nameCity;
            adb.setMessage(R.string.change_city_to);
            adb.setIcon(R.drawable.ic_geomarket);
            adb.setPositiveButton(R.string.reject,myOnlickListener);
            adb.setNegativeButton(R.string.no,myOnlickListener);
            adb.setNeutralButton(R.string.yes,myOnlickListener);
            //adb.set
            adb.create();
            adb.show();
        }
        return "";
    }


}
