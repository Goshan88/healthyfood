package healthyfood.dolg.healthyfood_4;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dolg on 11.01.18.
 */

public class ListForAutoCompleate {

    private static ArrayList<String> TypeList = new ArrayList<String>();
    private static ArrayList<String> NameList = new ArrayList<String>();
    private static ArrayList<String> UnitList = new ArrayList<String>();
    private static ArrayList<String> LocatedList = new ArrayList<String>();

    static public void addType(String _type){
        Log.d(Constant.MY_TAG,"Type add: " + _type);
        TypeList.add( _type);
    }

    static public List<String> getType(){
        return TypeList;
    }

    static public void addName(String _type){
        NameList.add( _type);
    }

    static public List<String> getName(){
        return NameList;
    }

    static public void addUnit(String _type){
        UnitList.add( _type);
    }

    static public List<String> getUnit(){
        return UnitList;
    }

    static public void addAddress(String _type){
        LocatedList.add( _type);
    }

    static public List<String> getAddress(){
        return LocatedList;
    }

    //проверка если в списках нету пункта то заносим в этот пункт
    //.addName(ArrFood[i][j].getName());
    public static void writeStringInArray(int TypeArr,String _name){
        switch(TypeArr){
            case Constant.AR_NAME:
                    if (NameList == null){
                        addName(_name);
                        return;
                    }
                    for(String object: NameList){
                        if(object.equalsIgnoreCase(_name)){return;}
                    }
                    addName(_name);
                break;
            case Constant.AR_UNIT:
                if (UnitList == null){
                    addUnit(_name);
                    return;
                }
                for(String object: UnitList){
                    if(object.equalsIgnoreCase(_name)){return;}
                }
                addUnit(_name);
                break;
            case Constant.AR_Located:
                if (LocatedList == null){
                    addAddress(_name);
                    return;
                }
                for(String object: LocatedList){
                    if(object.equalsIgnoreCase(_name)){return;}
                }
                addAddress(_name);
                break;
            case Constant.AR_TYPE:
                if (TypeList == null){
                    addType(_name);
                    return;
                }
                for(String object: TypeList){
                    if(object.equalsIgnoreCase(_name)){return;}
                }
                addType(_name);
                break;
            default:
                break;
        }
    }
}
