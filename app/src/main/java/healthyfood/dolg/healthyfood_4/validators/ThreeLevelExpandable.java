package healthyfood.dolg.healthyfood_4.validators;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;

import healthyfood.dolg.healthyfood_4.Food;
import healthyfood.dolg.healthyfood_4.R;
import healthyfood.dolg.healthyfood_4.TypeFood;

/**
 * Created by Гоша on 27.01.2018.
 */

public class ThreeLevelExpandable  extends BaseExpandableListAdapter{

    public static int FIRST_LEVEL_COUNT = 6;
    public static int SECOND_LEVEL_COUNT = 4;
    public static int THIRD_LEVEL_COUNT = 20;

    private ArrayList<TypeFood> typeList;
    private Context context;

    public ThreeLevelExpandable(Context context, ArrayList<TypeFood> _typeList) {
        this.typeList = new ArrayList<TypeFood>();
        this.typeList.addAll(_typeList);
        this.context = context;
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        //return arg1;
        ArrayList<Food> foodArrayList = typeList.get(arg0).getFoodList();
        return foodArrayList.get(arg1);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Food food = (Food) getChild(groupPosition,childPosition);

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_row, null);
        }

        SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);

        secondLevelELV.setAdapter(new SecondLevelAdapter(context,food));
        //NameFood.setText(food.getCity().trim());
        secondLevelELV.setGroupIndicator(null);
        return secondLevelELV;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return SECOND_LEVEL_COUNT;
        ArrayList<Food> foodList = typeList.get(groupPosition).getFoodList();
        return foodList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        //return groupPosition;
        return typeList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        //return FIRST_LEVEL_COUNT;
        return typeList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //String type_food = (String) getGroup(groupPosition);
        TypeFood type_food = (TypeFood) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_first, null);
            TextView text = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
            //text.setText("FIRST LEVEL");
            //TextView heading = (TextView) _view.findViewById(R.id.heading);
            text.setText(type_food.getName().trim());
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

    class SecondLevelExpandableListView extends ExpandableListView {

    public SecondLevelExpandableListView(Context context) {
        super(context);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}

     class SecondLevelAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Food food;

    public SecondLevelAdapter(Context context, Food _food) {
        this.context = context;
        food = _food;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        //Food food = (Food) getChild(_groupPosition, _childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_third, null);
            TextView text = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
            text.setText(food.getName());
        }
        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_second, null);
            TextView text = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
            //text.setText("THIRD LEVEL");
            text.setText(food.getAddress());
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //return THIRD_LEVEL_COUNT;
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}


