package healthyfood.dolg.healthyfood_4.validators;

import healthyfood.dolg.healthyfood_4.Food;

/**
 * Created by Гоша on 13.02.2018.
 */

public class staticFood {
    private static Food staticFood;

    public static void setStaticFood(Food mFood){
        staticFood = mFood;
    }

    public  static Food getStaticFood(){
        return staticFood;
    }
}
