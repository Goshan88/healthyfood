package healthyfood.dolg.healthyfood_4;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dolg on 22.12.17.
 */

public class User_static {
    private static String name;
    private static String email=Constant.Nothing;
    private static boolean editUser;
    private static boolean editFood;
    private static boolean record; //есть запись в базу данных
    private static String editCity=Constant.Nothing;//город в котором можно добавлять продукты в корзину

    User_static(){
    }

    User_static(String mname, String memail, boolean muser, boolean mfood, boolean mrecord, String mcity){
        name = mname;
        email = memail;
        editUser = muser;
        editFood = mfood;
        record = mrecord;
        editCity = mcity;
    }

    public static void setUser(String _name, String _email){
        name = _name;
        email = _email;
        Log.d(Constant.MY_TAG,"setUser name:" + name + " email:" + email);
    }

    public static void  setUser(String _name, String _email, boolean _edit_food, boolean _edit_user,
             boolean _have_record, String _edit_city){
        name = _name;
        email = _email;
        editFood = _edit_food;
        editUser = _edit_user;
        record = _have_record;
        editCity = _edit_city;
    }

    public static boolean getEditFood(){
        return editFood;
    }

    public static boolean getEditUser(){
        return editUser;
    }

    public static boolean getRecord(){
        return record;
    }

    public static void setRecordState(boolean _record_state){
        record = _record_state;
    }

    public static String getEditCity(){
        return editCity;
    }

    public static String getName(){
        return name;
    }

    User_static(String _name, String _email){
        name = _name;
        email = _email;
    }

    public static  String getEmail(){
        return email;
    }

    public static String String(){
         String tmp = "User: name-" + name + ", email-" + email + ", edit_user-" + String.valueOf(editUser)
                + ", edit_food-"+ String.valueOf(editFood)+ ", have_record-"+ String.valueOf(record)
                + ", city_edit-" + editCity;

        return tmp;
    }

}
