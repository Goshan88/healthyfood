package healthyfood.dolg.healthyfood_4;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

public class SearchPlace extends AppCompatActivity {
    private String[] ArrCities = new String[9];
    private ListView lvCities;
    private String Mode;
    private PlaceAutocompleteFragment autocompleteFragment;
    private Cities City;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__city);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        setSupportActionBar(mActionBarToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mActionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Log.d(Constant.MY_TAG, "Click on back arrow");
                finish();
            }
        });

        //создаем класс для того чтобы читать данные
        City = new Cities(this,SearchPlace.this);

        //собираем данные из прошлой активити
        Bundle intent = getIntent().getExtras();
        Mode = intent.getString(Constant.PLACE_MODE);
        Log.d(Constant.MY_TAG,"Mode is: " + Mode);

        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        if(Mode.equals(Constant.SEARCH_CITY)){
            //ищем город
            searchCity();
        }else{
            //ищем магазин
            searchAddress();
        }

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                if(Mode.equals(Constant.SEARCH_CITY)){
                    Log.d("MY_TAG", "Place: " + place.getName());
                    City.setName(place.getName().toString());
                }else{
                    Log.d("MY_TAG", "Place: " + place.getName()+"Address: "+ place.getAddress());
                    Intent intent = new Intent();
                    intent.putExtra(Constant.RESULT_ADDRESS, place.getName()+", "+place.getAddress()); //value should be your string from the edittext
                    setResult(Constant.SEARCH_ACTIVITY, intent); //The data you want to send back
                }
                finish();

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.d("MY_TAG", "An error occurred: " + status);
            }
        });


    }

    private void searchCity(){
        //Log.d(Constant.MY_TAG,"City 0:" + Array_Cities.loadCity(0));
        if(City.loadCity(0).equals(Constant.Nothing)){
            //инициализируем массив с названием городов
            Log.d(Constant.MY_TAG,"Init cities array");
            String[] aCities =  getResources().getStringArray(R.array.cities);
            for(int i=0;i<aCities.length;i++){
                City.saveCity(aCities[i],i);
            }
        }else{
            //читаем массив названия городов из памяти
            Log.d(Constant.MY_TAG,"Read cities array");
            for(int i=0;i<Constant.CITY_ARRAY_SIZE;i++){
                ArrCities[i] = City.loadCity(i);
                //Log.d(Constant.MY_TAG,"Read city " + String.valueOf(i) +"; " + ArrCities[i]);
            }
        }


        // находим список
        lvCities = (ListView) findViewById(R.id.lvCities);
        // устанавливаем режим выбора пунктов списка
        lvCities.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ArrCities);
        // присваиваем адаптер списку
        lvCities.setAdapter(adapter);

        lvCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(Constant.MY_TAG, "itemClick: position = " + position + ", id = "
                        + id);

                Log.d(Constant.MY_TAG, "Click on city = " + ArrCities[((int)id)]);
                City.setName(ArrCities[((int)id)]);
                finish();
            }
        });


        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();

        autocompleteFragment.setFilter(typeFilter);

    }

    private void searchAddress(){
        final String ArrAddress[] = new String[ListForAutoCompleate.getAddress().size()];
        int count = 0;
        for(String object:ListForAutoCompleate.getAddress()){
            ArrAddress[count] = object;
            count = count +1;
            Log.d(Constant.MY_TAG,"Object: "+object + " Номер: " + String.valueOf(count));
        }

        // находим список
        lvCities = (ListView) findViewById(R.id.lvCities);
        // устанавливаем режим выбора пунктов списка
        lvCities.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ListForAutoCompleate.getAddress());
        // присваиваем адаптер списку
        lvCities.setAdapter(adapter);

        lvCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(Constant.MY_TAG, "itemClick: position = " + position + ", id = "
                        + id);

                Log.d(Constant.MY_TAG, "Click on adress= " + parent.toString() + ", "
                        + view.toString());
                Intent intent = new Intent();
                intent.putExtra(Constant.RESULT_ADDRESS, ArrAddress[((int)id)]); //value should be your string from the edittext
                setResult(Constant.SEARCH_ACTIVITY, intent); //The data you want to send back
                finish();
            }
        });


        //получаем координаты города
        if(Geocoder.isPresent()) {
            try {
                Geocoder gc = new Geocoder((this));
                List<Address> addresses = gc.getFromLocationName(City.getCity(), 5); // get the found Address Objects
                List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
                for (Address a : addresses) {
                    if (a.hasLatitude() && a.hasLongitude()) {
                        ll.add(new LatLng(a.getLatitude(), a.getLongitude()));
                        Log.d(Constant.MY_TAG, "Coordinate: Latitude-" + a.getLatitude() + " ,Longitude-" + a.getLongitude());
                        autocompleteFragment.setBoundsBias(new LatLngBounds(
                                new LatLng(a.getLatitude(), a.getLatitude()),
                                new LatLng(a.getLatitude(), a.getLatitude())));
                    }
                }
            } catch (Exception e) {
                Log.d(Constant.MY_TAG, "Geocoder get error:" + e.getMessage());
            }
        }

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT)
                .build();

        autocompleteFragment.setFilter(typeFilter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    public void onNothingSelected(AdapterView<?> parent) {
        Log.d(Constant.MY_TAG, "itemSelect: nothing");
    }

    @Override
    public void onResume(){
        super.onResume();
        //initArCity();
    }

}
/*intent.putExtra("name", etName.getText().toString());
setResult(RESULT_OK, intent);*/