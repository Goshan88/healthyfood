package healthyfood.dolg.healthyfood_4;

import android.util.Log;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by dolg on 23.12.17.
 */

public class Food {
    private String type;
    private String name;
    private String cost;
    private String currency;
    private String address;
    private String author;
    private String date;
    private String info;

    Food(){
    }

    Food(String mtype, String mname, String mcost, String mcurrency, String maddress, String mauthor,
         String mnote){
        this.type = mtype;
        this.name = mname;
        this.cost = mcost;
        this.currency = mcurrency;
        this.address = maddress;
        this.author = mauthor;
        //запрашиваем дату
        DateFormat df = new SimpleDateFormat("d MMM yyyy");
        String cur_date = df.format(Calendar.getInstance().getTime());
        this.date = cur_date;
        this.info = mnote;
    }

    public String getType(){
        return type;
    }


    public String getName(){
        return name;
    }


    public String getCost(){
        return cost;
    }


    public String getCurrency(){
        return currency;
    }


    public String getAddress(){
        return address;
    }

    public String getAuthor(){
        return author;
    }

    public String getDate(){
        return date;
    }

    public String getInfo(){
        return info;
    }
}
