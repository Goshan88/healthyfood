package healthyfood.dolg.healthyfood_4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dolg on 03.01.18.
 */

public class FoodListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private static ArrayList<TypeFood> typeList;
    private ArrayList<TypeFood> originalList;

    public FoodListAdapter(Context _context, ArrayList<TypeFood> _typeList) {
        this.context = _context;
        this.typeList = new ArrayList<TypeFood>();
        this.typeList.addAll(_typeList);
        this.originalList = new ArrayList<TypeFood>();
        this.originalList.addAll(_typeList);
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Food> foodArrayList = typeList.get(groupPosition).getFoodList();
        return foodArrayList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int _groupPosition, int _childPosition, boolean _isLastChild,
                             View _view, ViewGroup _parent) {
        Food food = (Food) getChild(_groupPosition, _childPosition);

        SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);
        secondLevelELV.setAdapter(new SecondLevelAdapter(context,food));
        return secondLevelELV;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Food> foodList = typeList.get(groupPosition).getFoodList();
        return foodList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return typeList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return typeList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View _view,
                             ViewGroup _parent) {
        TypeFood type_food = (TypeFood) getGroup(groupPosition);
        if (_view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            _view = layoutInflater.inflate(R.layout.row_first, null);
        }
        TextView heading = (TextView) _view.findViewById(R.id.eventsListEventRowText);
        heading.setText(type_food.getName().trim());

        return _view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void filterData(String query) {
        query = query.toLowerCase();
        Log.d("MY_TAG", "MyListAdapter: " + String.valueOf(typeList.size()));
        typeList.clear();

        if (query.isEmpty()) {
            typeList.addAll(originalList);
        } else {
            for (TypeFood typeFood : originalList) {
                ArrayList<Food> foodList = typeFood.getFoodList();
                ArrayList<Food> newList = new ArrayList<Food>();
                for (Food food : foodList) {
                    Log.d("MY_TAG", "Food name: " + food.getName().toLowerCase().contains("и"));
                    if (food.getName().toLowerCase().contains(query)) {
                        newList.add(food);
                    }
                }
                if (newList.size() > 0) {
                    TypeFood n_type_food = new TypeFood(typeFood.getName(), newList);
                    typeList.add(n_type_food);
                }
            }
        }
        Log.d("MY_TAG", "MyListAdapter: " + String.valueOf(typeList.size()));
        notifyDataSetChanged();
    }


    class SecondLevelExpandableListView extends ExpandableListView {

        public SecondLevelExpandableListView(Context context) {
            super(context);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    class SecondLevelAdapter extends BaseExpandableListAdapter {

        private Context context;
        private Food food;

        public SecondLevelAdapter(Context context, Food _food) {
            this.context = context;
            food = _food;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public int getGroupCount() {
            return 1;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            //Food food = (Food) getChild(_groupPosition, _childPosition);
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_second, null);
                TextView name_of_food = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
                TextView cost_of_food = (TextView) convertView.findViewById(R.id.cost_of_food);
                name_of_food.setText(food.getName());
                cost_of_food.setText(food.getCost()/*+" "+food.getCurrency()*/);
            }
            return convertView;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_third, null);
                TextView address = (TextView) convertView.findViewById(R.id.eventsListEventRowText);
                TextView info = (TextView) convertView.findViewById(R.id.info_about_food);
                TextView auth = (TextView) convertView.findViewById(R.id.auth_food);
                address.setText(food.getAddress());
                info.setText(food.getInfo());
                //Log.d(Constant.MY_TAG,"Info about food: "+ food.getInfo());
                auth.setText(food.getAuthor());
            }
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            //return THIRD_LEVEL_COUNT;
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    public static  ArrayList<TypeFood> getArrayList(){
        return  typeList;
    }
}