package healthyfood.dolg.healthyfood_4;

import android.os.Handler;

import java.util.TimerTask;

/**
 * Created by Гоша on 24.01.2018.
 */

public class UpdateTimer extends TimerTask {

    Handler h;
    int Commnd;
    UpdateTimer(Handler _handler, int _handler_command){
        h= _handler;
        Commnd = _handler_command;
    }
    public void run(){
        h.sendEmptyMessage(Commnd);
    }
}
