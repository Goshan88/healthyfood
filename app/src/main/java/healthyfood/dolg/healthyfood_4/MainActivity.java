package healthyfood.dolg.healthyfood_4;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.auth.api.Auth;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import healthyfood.dolg.healthyfood_4.validators.staticFood;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {
    //private Drawer.Result drawerResult = null;

    public DataBase FirebaseRTD;

    private MenuItem editMenuItem, UserLogIn;
    private MenuItem newMenuItem;
    private MenuItem removeMenuItem;
    private MenuItem searchMenuItem;
    private MenuItem closeMenuItem;
    private MenuItem enterMenuItem;
    private int ListMode = Constant.ListNothing;//режим работы с листом: удаление, правка и т.д
    private int PositionClick = 0;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirechatUser;
    private String mUsername;
    private String mPhotoUrl;

    private ImageButton LogOut, LogIn, iCity;
    private View LogIn_View, user_photo;
    private TextView LogIn_Text, name_user, email_user, TitleCity, EmptyDB;
    private String City_Name;
    private AccountHeader headerResult;
    private Uri photo_user_uri;
    private Bitmap photo_user_btm;
    private Drawer result;
    private PrimaryDrawerItem item1;
    private PrimaryDrawerItem item2;
    private PrimaryDrawerItem item3;
    private PrimaryDrawerItem item4;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    public ProgressBar load_database;
    private ExpandableListView myList;

    private Handler h;
    public Timer timer;
    public Cities Array_Cities;
    private int CountUpdate = 0;
    private AlertDialog.Builder adb;
    private int multi_choice_lenth = 0;
    boolean checkbox_state[] = new boolean[Constant.MAX_SIZE_NAME];
    private Context myContext;
    private Admob Advertisment;
    private User user;
    private DataBase UsersDB;
    private  Timer user_timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //search bar
        Toolbar action_toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(action_toolbar);


        UsersDB = new DataBase(this,Constant.NODE_USERS);
        EmptyDB = (TextView) findViewById(R.id.city_db_empty);

        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case Constant.HANDLER_UPDATE_TIME:
                        CountUpdate = CountUpdate + 1;
                        Log.d(Constant.MY_TAG, "CountUpdate: " + String.valueOf(CountUpdate));
                        Array_Cities.getLocated();
                        String nameCity = Array_Cities.getCity_Name(Cities.getLocation());
                        if (nameCity.equals(Constant.Nothing)) {
                            if (CountUpdate > Constant.MAX_COUNT_UPDATE) {
                                //Array_Cities.initCity(Constant.Nothing);
                                timer.cancel();//останавливаем таймер
                            }
                        } else {
                            City_Name = Array_Cities.initCity(nameCity);
                            timer.cancel();//останавливаем таймер
                        }
                        break;
                    case Constant.HANDLER_UPDATE_EXPN:
                        FirebaseRTD = new DataBase(MainActivity.this, Array_Cities.getCity(), myList, load_database,h);
                        createNavigatorPanel();
                        load_database.setVisibility(View.VISIBLE);
                        myList.setVisibility(View.GONE);
                        TitleCity.setText(Cities.getCityName());
                        break;
                    case Constant.HANDLER_USER:
                        Log.d(Constant.MY_TAG, "Load user profile from DataBase: ");
                        //если пользователь есть
                        user.setUser(User_static.getName(),User_static.getEmail(),User_static.getEditUser(),
                                User_static.getEditFood(),User_static.getRecord(),User_static.getEditCity());
                        write_user();
                        toolbar_home();
                        Log.d(Constant.MY_TAG, "Users load from DB: " + user.String());
                        break;
                    case Constant.HANDLER_USER_CANCEL:
                        //если пользователя не нашли в базе данных
                        Log.d(Constant.MY_TAG, "Users haven't in DB");
                        //записываем пользователя в БД
                        User new_user = new User(mFirechatUser.getDisplayName().toString(), mFirechatUser.getEmail().toString());
                        UsersDB.writeUserToDB(new_user);
                        break;
                    case Constant.HANDLER_USER_UPDATE:
                        Log.d(Constant.MY_TAG, "Users timer stop");
                        user_timer.cancel();//останавливаем таймер
                        //если не нашли запись о пользователи то добавляем ее
                        if(user.getRecord()==false){
                            User new_user_1 = new User(mFirechatUser.getDisplayName().toString(), mFirechatUser.getEmail().toString());
                            UsersDB.writeUserToDB(new_user_1);
                        }
                        break;
                    case Constant.HANDLER_CITY_EMPTY://если в БД нет записисей об этом городе
                        EmptyDB.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        };

        //инициализация пользователя
        initUser();
        myContext =this;

        Array_Cities = new Cities(this, MainActivity.this, h);
        Array_Cities.getPermission();

        //рисуем список данных БД
        myList = (ExpandableListView) findViewById(R.id.expandableList);
        load_database = (ProgressBar) findViewById(R.id.pbHeaderProgress);
        Log.d(Constant.MY_TAG, "Cities.getCity: " + Array_Cities.getCity());
        FirebaseRTD = new DataBase(this, Array_Cities.getCity(), myList, load_database,h);

        //создаем панель навигации
        createNavigatorPanel();

        //создаем класс для обновления данных
        timer = new Timer();
        TimerTask UpdateDate = new UpdateTimer(h,Constant.HANDLER_UPDATE_TIME);
        timer.scheduleAtFixedRate(UpdateDate, Constant.PERIOD_TIMER, Constant.PERIOD_TIMER);

        myList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Log.d(Constant.MY_TAG, "ArrayTypeFood " + String.valueOf(FoodListAdapter.getArrayList().get((int) id).getName()));
                switch (ListMode) {
                    case Constant.ListNothing:
                        Log.d(Constant.MY_TAG, "Click with mode Nothing");
                        return false;//если просто клацают по списку
                    case Constant.ListEdit:
                        Log.d(Constant.MY_TAG, "Click with mode Edit");
                        break;
                    case Constant.ListRemowe:
                        Log.d(Constant.MY_TAG, "Click with mode Remove");
                        break;
                    default:
                        break;
                }
                PositionClick = (int) id;
                Log.d(Constant.MY_TAG, "Expandable Click: " + String.valueOf(PositionClick));
                //showDialog(ListMode);//к нечему не обязывающая константа
                showWindowDialog();
                return false;
            }
        });

       /*почему то не работает
        myList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandablelistview,
               View clickedView, int groupPosition, int childPosition, long childId) {
                Log.d(Constant.MY_TAG, "onGroupClick groupPosition = " + groupPosition +
                        " childPosition = " + childPosition + " childId = " + childId);
                return true;
                                                       }
        });*/

    /*почемуто не работает скрывание бокового меню
    @Override
    public void onBackPressed(){
        if(Drawer.isDrawerOpen()){
            drawerResult.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }*/
        //грузим рекламу
        Advertisment = new Admob();
        Advertisment.setViewAdvertisment(true);
        AdView mAdView = findViewById(R.id.adView);
        Advertisment.BannerView(mAdView);
        Advertisment.InterstitialView(this);

        //заголовок с названием города
        View.OnClickListener lestTitleCity = new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent city_int = new Intent(MainActivity.this, SearchPlace.class);
                city_int.putExtra(Constant.PLACE_MODE, Constant.SEARCH_CITY);
                startActivityForResult(city_int, Constant.SEARCH_ACTIVITY);
            }
        };
            iCity = (ImageButton) findViewById(R.id.ibut_city);
            TitleCity = (TextView) findViewById(R.id.title_city);
            iCity.setOnClickListener(lestTitleCity);
            TitleCity.setOnClickListener(lestTitleCity);
            TitleCity.setText(Array_Cities.getCity());
    }

    //ининциализация пользователя
    private void initUser() {
        user = new User();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirechatUser = mFirebaseAuth.getCurrentUser();
        if (mFirechatUser == null) {
            user.setUser(Constant.DEFAULT_NAME,Constant.DEFAULT_EMAIL);
            return;
        } else {
            mUsername = mFirechatUser.getDisplayName();
            if (mFirechatUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirechatUser.getPhotoUrl().toString();
            }
            user.setUser(mFirechatUser.getDisplayName(), mFirechatUser.getEmail());
            photo_user_uri = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();   //пока не работает
            //photo_user_btm = User.getBitmapFromURL(photo_user_uri);задача долгая тяжеловесная так что выдает ошибку
            //Log.d(Constant.MY_TAG, "initUser Uri: " + photo_user_uri.toString());
            Log.d(Constant.MY_TAG, "User: " + mFirechatUser.getDisplayName().toString());
            Log.d(Constant.MY_TAG, "Email: " + mFirechatUser.getEmail().toString());
        }

        //check user permission
        if(user.getEmail().equals(Constant.DEFAULT_EMAIL)){
            //пользователь не авторизован
        }else{
            if(user.getRecord()==false) {
                //проверяем есть ли пользовательс таким электронным адресом
                Log.d(Constant.MY_TAG, "User before read: " + user.String());
                UsersDB.readUserFromBD(user, h);
                //запускаем таймер для проверки чтения данных о пользователи из БД в течение 5 секунд
                user_timer = new Timer();
                TimerTask UpdateUser = new UpdateTimer(h, Constant.HANDLER_USER_UPDATE);
                Log.d(Constant.MY_TAG, "Users timer start");
                user_timer.scheduleAtFixedRate(UpdateUser,Constant.PERIOD_USER_TIMER,Constant.PERIOD_USER_TIMER );
            }
        }

    }

    //навигатор бар слева
    private void createNavigatorPanel() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorPrimary)
                .build();
        LogOut = (ImageButton) headerResult.getView().findViewById(R.id.logout);
        LogIn = (ImageButton) headerResult.getView().findViewById(R.id.login);
        LogIn_Text = (TextView) headerResult.getView().findViewById(R.id.login_text);
        LogIn_View = (View) headerResult.getView().findViewById(R.id.login_view);
        user_photo = (View) headerResult.getView().findViewById(R.id.material_drawer_account_header_current);
        name_user = (TextView) headerResult.getView().findViewById(R.id.material_drawer_account_header_name);
        email_user = (TextView) headerResult.getView().findViewById(R.id.material_drawer_account_header_email);

        // создаем обработчик нажатия
        write_user();
        View.OnClickListener oclLogOut = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Меняем текст в TextView (tvOut)
                Log.d(Constant.MY_TAG, "Header navigatorpanel click:");
                switch (v.getId()) {
                    case R.id.logout:
                        Log.d(Constant.MY_TAG, "LogOut click");
                        mFirebaseAuth.signOut();
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                        user.setUser(Constant.DEFAULT_NAME, Constant.DEFAULT_EMAIL);
                        write_user();
                        toolbar_home();
                        break;
                    case R.id.login_text:
                    case R.id.login:
                        Log.d(Constant.MY_TAG, "LogIn click");
                        Intent intent = new Intent(MainActivity.this, Authorization.class);
                        startActivity(intent);
                        user.setUser(Constant.DEFAULT_NAME, Constant.DEFAULT_EMAIL);
                        break;
                    default:
                        break;
                }
            }
        };
        LogOut.setOnClickListener(oclLogOut);
        LogIn.setOnClickListener(oclLogOut);

        item1 = new PrimaryDrawerItem().withName(Array_Cities.getCity()).withIcon(GoogleMaterial.Icon.gmd_navigation);
        item2 = new PrimaryDrawerItem().withName(R.string.settings).withIcon(GoogleMaterial.Icon.gmd_settings);
        item3 = new PrimaryDrawerItem().withName(R.string.help).withIcon(GoogleMaterial.Icon.gmd_help);
        item4 = new PrimaryDrawerItem().withName(R.string.support).withIcon(GoogleMaterial.Icon.gmd_mail);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        item1,
                        item2,
                        item3,
                        item4
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    // Обработка клика
                    public boolean onItemClick(View view, int id, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Log.d(Constant.MY_TAG, "ID click item left menu: " + String.valueOf(id));
                            switch ((int) id) {
                                case Constant.LEFT_ITEM_CITY:
                                    Intent city_int = new Intent(MainActivity.this, SearchPlace.class);
                                    city_int.putExtra(Constant.PLACE_MODE, Constant.SEARCH_CITY);
                                    startActivityForResult(city_int, Constant.SEARCH_ACTIVITY);
                                    break;
                                case Constant.LEFT_ITEM_SETTINGS:
                                    Intent settings_int = new Intent(MainActivity.this, Settings.class);
                                    startActivity(settings_int);
                                    break;
                                default:
                                    Toast.makeText(MainActivity.this, "id: " + String.valueOf(id), Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                        return true;
                    }
                })
                .build();
        City_Name = Array_Cities.getCity();
    }


    //пишет юзера в тулбаре
    private void write_user() {
        //пищем текущего пользователя или предлагаем авторизоваться
        Log.d(Constant.MY_TAG, "write_user User Name: " + user.getName());
        if (headerResult.getActiveProfile() != null) {
            headerResult.removeProfile(headerResult.getActiveProfile());
            headerResult.clear();
        }
        if (user.getName().equals(Constant.DEFAULT_NAME)) {
            //если пользователь не авторизован
            LogOut.setVisibility(View.GONE);
            name_user.setVisibility(View.GONE);
            email_user.setVisibility(View.GONE);
            user_photo.setVisibility(View.GONE);
            LogIn.setVisibility(View.VISIBLE);
            LogIn_Text.setVisibility(View.VISIBLE);
            LogIn_View.setVisibility(View.VISIBLE);
        } else {
            //если пользователь авторизоваля то пищем его данные
            LogIn.setVisibility(View.GONE);
            LogIn_Text.setVisibility(View.GONE);
            LogIn_View.setVisibility(View.GONE);
            user_photo.setVisibility(View.VISIBLE);
            LogOut.setVisibility(View.VISIBLE);
            name_user.setVisibility(View.VISIBLE);
            email_user.setVisibility(View.VISIBLE);
            headerResult.addProfiles(
                    new ProfileDrawerItem().withName(user.getName()).withEmail(user.getEmail()).withIcon(photo_user_uri)
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        newMenuItem = menu.findItem(R.id.action_new);
        removeMenuItem = menu.findItem(R.id.action_remove);
        searchMenuItem = menu.findItem(R.id.action_search);
        editMenuItem = menu.findItem(R.id.action_edit);
        closeMenuItem = menu.findItem(R.id.action_close);
        enterMenuItem = menu.findItem(R.id.action_enter);
        UserLogIn = menu.findItem(R.id.user_login);
        enterMenuItem.setVisible(false);
        toolbar_home();
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i(Constant.MY_TAG, "Text from search: " + newText);
                FirebaseRTD.listAdapter.filterData(newText);
                FirebaseRTD.expandAll();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Log.d(Constant.MY_TAG, "Search action");
            return true;
        } else if (id == R.id.action_edit) {
            Log.d(Constant.MY_TAG, "Edit action");
            toolbar_close();
            ListMode = Constant.ListEdit;
            return true;
        } else if (id == R.id.action_new) {
            Log.d(Constant.MY_TAG, "New action");
            Log.d(Constant.MY_TAG, "Type autocompleate" + ListForAutoCompleate.getType());
            Intent intent = new Intent(myContext, FoodEditor.class);
            intent.putExtra(Constant.EDITOR_MODE,Constant.FOOD_ADD);
            intent.putExtra(Constant.EDITOR_NAME,user.getName());
            intent.putExtra(Constant.EDITOR_EMAIL,user.getEmail());
            startActivity(intent);
            return true;
        } else if (id == R.id.action_remove) {
            Log.d(Constant.MY_TAG, "Remove action");
            toolbar_close();
            ListMode = Constant.ListRemowe;
            return true;
        } else if (id == R.id.action_close) {
            Log.d(Constant.MY_TAG, "Close action");
            toolbar_home();
            return true;
        } else if (id == R.id.action_enter) {
            Log.d(Constant.MY_TAG, "Enter action");
            toolbar_home();
            return true;
        }else if (id == R.id.user_login) {
            Log.d(Constant.MY_TAG, "User login");
            startActivity(new Intent(this, Authorization.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toolbar_home() {

        if(user.getEmail().equals(Constant.DEFAULT_EMAIL)){
            //пользователь не авторизован
            hide_action_toolbar();
            UserLogIn.setVisible(true);
        }else{
            //проверяем права пользователя на изменения в продукты
           if(user.getEditFood()==true){
                if(user.getEditCity().equals(Cities.getCityName())) {
                    show_action_toolbar();
                }else{
                    hide_action_toolbar();
                }
            }else{
               hide_action_toolbar();
           }
            UserLogIn.setVisible(false);
        }
        ListMode = Constant.ListNothing;
        searchMenuItem.setVisible(true);
        closeMenuItem.setVisible(false);
    }

    private void hide_action_toolbar(){
        editMenuItem.setVisible(false);
        newMenuItem.setVisible(false);
        removeMenuItem.setVisible(false);
    }

    private void show_action_toolbar(){
        editMenuItem.setVisible(true);
        newMenuItem.setVisible(true);
        removeMenuItem.setVisible(true);
    }

    private void toolbar_close() {
        hide_action_toolbar();
        searchMenuItem.setVisible(false);
        closeMenuItem.setVisible(true);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(Constant.MY_TAG, "MainActivity onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(Constant.MY_TAG, "MainActivity onResume");
        Log.d(Constant.MY_TAG, "City_name: " + City_Name + ", getCity: " + Array_Cities.getCity());
        Log.d(Constant.MY_TAG, "User name: " + user.getName());
        //проверяем если поменяли город то переписываем toolbar
        if (City_Name.equals(Array_Cities.getCity())) {
            //если город остался прежним то ничего не делаем
        } else {
            //если поменялся
            item1 = new PrimaryDrawerItem().withName(Array_Cities.getCity()).withIcon(GoogleMaterial.Icon.gmd_navigation);
            result.updateItemAtPosition(item1, 1);
            load_database.setVisibility(View.VISIBLE);
            myList.setVisibility(View.GONE);
            TitleCity.setText(Array_Cities.getCity());
            toolbar_home();
            EmptyDB.setVisibility(View.GONE);
        }
        //рисуем список данных БД
        FirebaseRTD = new DataBase(this, Array_Cities.getCity(), myList, load_database,h);
    }

    //диалог для удаления или изменения
    //protected Dialog onCreateDialog(int id){
    private void showWindowDialog() {
        adb = new AlertDialog.Builder(this, R.style.AlertDialogStyle);

        switch (ListMode) {
            case Constant.ListRemowe:
                // заголовок
                adb.setTitle(R.string.dialog_title_remove);
                // сообщение
                //adb.setMessage("удалить позиции");
                adb.setIcon(android.R.drawable.ic_delete);
                ArrayList<Food> foods = FoodListAdapter.getArrayList().get(PositionClick).getFoodList();
                Log.d(Constant.MY_TAG, "Food type click = " + FoodListAdapter.getArrayList().get(PositionClick).getName());
                multi_choice_lenth = foods.size();
                String food_name[] = new String[(foods.size() + 1)];
                int count = 0;
                food_name[count] = FoodListAdapter.getArrayList().get(PositionClick).getName();//выводим имя класса для того чтобы его удалить
                count = count + 1;
                for (Food f : foods) {
                    food_name[count] = f.getName();
                    count = count + 1;
                    Log.d(Constant.MY_TAG, "Array food name = " + food_name[(count - 1)]);
                }
                for(int i=0;i<checkbox_state.length;i++) checkbox_state[i]=false;
                adb.setMultiChoiceItems(food_name, checkbox_state, myItemsMultiClickListener);
                break;
            case Constant.ListEdit:
                adb.setTitle(R.string.dialog_title_edit);
                ArrayList<Food> food_edit = FoodListAdapter.getArrayList().get(PositionClick).getFoodList();
                String food_name_edit[] = new String[food_edit.size()];
                int count_edit = 0;
                food_name_edit[count_edit] = FoodListAdapter.getArrayList().get(PositionClick).getName();//выводим имя класса для того чтобы его удалить
                multi_choice_lenth = food_edit.size();
                for (Food f : food_edit) {
                    food_name_edit[count_edit] = f.getName();
                    count_edit = count_edit + 1;
                    Log.d(Constant.MY_TAG, "Array food name edit= " + food_name_edit[(count_edit - 1)]);
                }
                for(int i=0;i<checkbox_state.length;i++) checkbox_state[i]=false;
                adb.setMultiChoiceItems(food_name_edit, checkbox_state, myItemsMultiClickListener);
                break;
        }

        adb.setPositiveButton(R.string.dialog_yes, myBtnClickListener);
        adb.setNeutralButton(R.string.dialog_cancel, myBtnClickListener);
        adb.show();
    }


    // обработчик нажатия на пункт списка диалога или кнопку
   /*DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            ListView lv = ((AlertDialog) dialog).getListView();
            if (which == Dialog.BUTTON_POSITIVE)
            // выводим в лог позицию выбранного элемента
            Log.d(Constant.MY_TAG, "pos = " + lv.getCheckedItemPosition());
            else
            // выводим в лог позицию нажатого элемента
            Log.d(Constant.MY_TAG, "which = " + which);
        }
    };*/

    //обработчик для списка массива
    DialogInterface.OnMultiChoiceClickListener myItemsMultiClickListener = new DialogInterface.OnMultiChoiceClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            Log.d(Constant.MY_TAG, "which = " + which + ", isChecked = " + isChecked);
            switch (ListMode) {
                case Constant.ListRemowe:
                    if (which == 0) {//если клик на тип, то выбираем все
                        if (isChecked == true) {//если выбран
                            for (int i = 1; i < (multi_choice_lenth + 1); i++) {
                                ((AlertDialog)dialog).getListView().setItemChecked(i, true);
                                checkbox_state[i] = true;
                            }
                        } else {
                            for (int i = 1; i < (multi_choice_lenth + 1); i++) {
                                ((AlertDialog)dialog).getListView().setItemChecked(i, false);
                                checkbox_state[i] = false;
                            }
                        }
                    } else {

                    }
                    break;
                case Constant.ListEdit:
                    Log.d(Constant.MY_TAG, "Edit punct which = " + which + ", isChecked = " + isChecked);
                    for (int i = 0; i < (multi_choice_lenth); i++) {
                        ((AlertDialog)dialog).getListView().setItemChecked(i, false);
                        checkbox_state[i] = false;
                    }
                    ((AlertDialog)dialog).getListView().setItemChecked(which, true);
                    checkbox_state[which] = true;
                    break;
            }
        }
    }

        ;
        // обработчик для списка курсора
        DialogInterface.OnMultiChoiceClickListener myCursorMultiClickListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                ListView lv = ((AlertDialog) dialog).getListView();
                Log.d(Constant.MY_TAG, "which = " + which + ", isChecked = " + isChecked);
            }
        };

        DialogInterface.OnClickListener myBtnClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_POSITIVE:
                        Log.d(Constant.MY_TAG, "Click positive btn ListMode: " + String.valueOf(ListMode));
                        Log.d(Constant.MY_TAG, "ListMode: " + String.valueOf(ListMode));
                        ArrayList<Food> foods = FoodListAdapter.getArrayList().get(PositionClick).getFoodList();
                        switch (ListMode){
                            case Constant.ListRemowe:
                                for (int i = 1; i < (multi_choice_lenth+1); i++) {
                                    if (checkbox_state[i] == true){
                                        Log.d(Constant.MY_TAG, "Delete data: " + foods.get((i-1)).getName());
                                        FirebaseRTD.deleteData(foods.get((i-1)).getName());
                                        myList.setAdapter((BaseExpandableListAdapter)null);
                                    }
                                }
                                break;
                            case Constant.ListEdit:
                                //ищем отмеченный пункт меню
                                Log.d(Constant.MY_TAG, "Case edit food activity");
                                int numb_check = 0;
                                for (int i = 0; i < multi_choice_lenth; i++) {
                                    if (checkbox_state[i] == true){
                                        numb_check = i;
                                    }
                                }
                                staticFood.setStaticFood(foods.get(numb_check));
                                Log.d(Constant.MY_TAG, "Edit food name" + foods.get(numb_check).getName());
                                Intent intent = new Intent(myContext, FoodEditor.class);
                                intent.putExtra(Constant.EDITOR_MODE,Constant.FOOD_EDIT);
                                intent.putExtra(Constant.EDITOR_NAME,user.getName());
                                intent.putExtra(Constant.EDITOR_EMAIL,user.getEmail());
                                startActivity(intent);
                                break;
                            default:
                                break;
                        }
                        break;
                    case Dialog.BUTTON_NEUTRAL:
                        Log.d(Constant.MY_TAG, "Click neutral btn");
                        break;
                    default:
                        break;
                }
                toolbar_home();
            }
        };
    }