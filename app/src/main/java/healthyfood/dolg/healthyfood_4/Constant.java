package healthyfood.dolg.healthyfood_4;

/**
 * Created by dolg on 05.01.18.
 */

public interface Constant {
    final String MY_TAG = "MY_TAG";
    final String Nothing = ".";
    int MIN_SIZE_VALIDATORS = 3;
    int MAX_SIZE_VALIDATORS = 50;
    String DEFAULT_NAME = "anonimus";
    String DEFAULT_EMAIL= "default@mail.ru";

    int AR_NAME = 0;//массив имен
    int AR_UNIT = 1;//массив единиц
    int AR_Located=2;//массив адресов
    int AR_TYPE=3;//массив типов продуктов

    String CITY_DEFAULT_NAME = "Уфа";
    String PREF_NAME = "city ";
    String PREF_USER = "user ";
    String PREF_AUTOGET_LOC = "auto_get";
    int CITY_ARRAY_SIZE = 9;

    int LEFT_ITEM_CITY = 1;
    int LEFT_ITEM_SETTINGS = 2;
    int LEFT_ITEM_HELP = 3;
    int LEFT_ITEM_MAIL = 4;

    int SEARCH_ACTIVITY = 1;

    String PLACE_MODE = "search_place";//режим поиска города или адреса
    String SEARCH_CITY = "search_city";//ищем магазин
    String SEARCH_MARKET = "market";//ищем магазин
    String RESULT_ADDRESS = "address_market";//ищем магазин

    double LAT_DEFAULT = -1.00;
    double LON_DEFAULT = -1.00;

    int HANDLER_UPDATE_TIME = 1;
    int HANDLER_UPDATE_EXPN = 2;
    int HANDLER_USER = 3;
    int HANDLER_USER_CANCEL = 4;
    int HANDLER_USER_UPDATE = 5;//таймер для запроса данных пользователя у БД
    int HANDLER_CITY_EMPTY = 6;
    int START_TIMER = 0;
    int PERIOD_TIMER = 1000;//1сек
    int PERIOD_USER_TIMER = 5000;//5сек

    //то где нужно иенять значение констант
    int MAX_SIZE_TYPE =100; //максимальное колич типов продуктов
    int MAX_SIZE_NAME =50; //максимальное колличество названий в типе
    int MAX_COUNT_UPDATE = 3; //максимальное количество перезапросов

    int ListNothing=0;//ничего не делать
    int ListRemowe=1;//удалить продукт или тип
    int ListEdit=2;//изменить продукт
    int REMOVE_ITEM = 0;
    int EDIT_ITEM = 1;

    String FOOD_EDIT = "EDIT_MODE";
    String FOOD_ADD = "ADD_MODE";
    String EDITOR_MODE = "editor_mode";
    String EDITOR_NAME = "editor_name";
    String EDITOR_EMAIL = "editor_email";
    String ORDER_QUERY = "name";//упорядочить сптсок по этому пункту

    String ADMOB_BANNER  =  "ca-app-pub-6912796345298703/3571246798";
    String ADMOB_INTERSTITIAL = "ca-app-pub-6912796345298703/5419229671";
    String ADMOB_USER_ID = "ca-app-pub-6912796345298703~6963696892";

    String NODE_USERS = "123_users";//для того чтобы не соавло с названием города
    String ORDER_EMAIL = "email";

    //постояннве строки для поиска переменных из prefab
    String USER_NAME = "name-";
    String USER_EMAIL = "email-";
    String USER_EDIT_USER = "edit_user-";
    String USER_EDIT_FOOD = "edit_food-";
    String USER_EDIT_RECORD = "have_record-";
    String USER_EDIT_CITY = "city_edit-";
}
