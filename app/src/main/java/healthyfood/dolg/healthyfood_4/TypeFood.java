package healthyfood.dolg.healthyfood_4;

import java.util.ArrayList;

/**
 * Created by dolg on 03.01.18.
 */

public class TypeFood extends ArrayList {
    private String name;
    private ArrayList<Food> foodList = new ArrayList<Food>();

    public TypeFood(String _name, ArrayList<Food> _foodList){
        super();
        this.name = _name;
        this.foodList =_foodList;
    }

    public String getName(){
        return name;
    }

    public void setName(String _name){
        this.name = _name;
    }

    public ArrayList<Food> getFoodList(){
        return foodList;
    }

    public void setFoodList(ArrayList<Food> _foodList){
        this.foodList = _foodList;
    }

    public String outputTypeFoodList(ArrayList<Food> _foodList){
        StringBuilder sb = new StringBuilder();
        for (Food food: _foodList){
            sb.append(food.getName());
            sb.append(",");
            sb.append(food.getType());
            sb.append("; ");
        }

        return sb.toString();
    }
}
