package healthyfood.dolg.healthyfood_4;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import healthyfood.dolg.healthyfood_4.R;

/**
 * Created by Гоша on 19.02.2018.
    Данный класс написан для управления рекламой на странице, для ее отключения в случае покупки
 */

public class Admob extends AppCompatActivity {
    private  boolean view_admob = true;
    private InterstitialAd mInterstitialAd;

    public void BannerView(AdView _Adview){

        if(view_admob == true) {
            _Adview.setVisibility(View.VISIBLE);
            AdRequest adRequest = new AdRequest.Builder().build();
            _Adview.loadAd(adRequest);
        }else{
            _Adview.setVisibility(View.GONE);
        }
    }

    public void InterstitialView(Context myContext){

        if(view_admob == true) {
            MobileAds.initialize(myContext,Constant.ADMOB_USER_ID);
            mInterstitialAd = new InterstitialAd(myContext);
            mInterstitialAd.setAdUnitId(Constant.ADMOB_INTERSTITIAL);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }else{
        }
    }

    public void setViewAdvertisment(boolean _set){
        view_admob = _set;
    }
}
