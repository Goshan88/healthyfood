package healthyfood.dolg.healthyfood_4;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import healthyfood.dolg.healthyfood_4.validators.staticFood;

/**
 * Created by dolg on 05.01.18.
 */

public class DataBase extends AppCompatActivity{
    private Context context;
    private DatabaseReference mDatabase;
    private String CityName=Constant.CITY_DEFAULT_NAME;


    //коллекция для групп
    ArrayList<Map<String,String>> TypeFood;
    //коллекция для элементов одной группы
    ArrayList<Map<String, String>> nameFood;
    //общая коллекция для коллекций элементов
    ArrayList<ArrayList<Map<String,String>>> childFood;
    //список атрибутов группы для элемента
    Map<String, String> m;
    Map<String, String> n;


    //сосдание массива для дерева отображения спсика еды
    int count_type =0;//считает количество типов продуктов
    boolean have_type = false; //такой тип уже был
    Food [][] ArrFood;

    private ArrayList<TypeFood> typeList = new ArrayList<TypeFood>();
    public FoodListAdapter listAdapter;
    public ExpandableListView myList;
    public ProgressBar load_database;
    private Handler h;

    public DataBase(Context _context, String _query_ref){
        context = _context;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference(_query_ref);
        CityName = _query_ref;
    }

    public DataBase(Context _context, String _query_ref, ExpandableListView _myList, ProgressBar _load_database, Handler _h){
        context = _context;
        myList = _myList;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference(_query_ref);
        typeList = new ArrayList<TypeFood>();
        init_database();
        Log.d(Constant.MY_TAG,"Init database _query_ref: " + _query_ref);
        load_database = _load_database;
        h=_h;
    }

    void init_database() {
        ArrFood = new Food[Constant.MAX_SIZE_TYPE][Constant.MAX_SIZE_NAME];
        //ArrTypeFood = new TypeFood[Constant.MAX_SIZE_TYPE];
        //cleanArr();
        //создаем коллекцию групп
        TypeFood = new ArrayList<Map<String, String>>();
        //создаем коллекцию для коллекций элементов
        childFood = new ArrayList<ArrayList<Map<String, String>>>();
        //создаем коллекцию элементов для групп
        nameFood = new ArrayList<Map<String, String>>();

        initListener();
    }

    private void initListener() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                load_database.setVisibility(View.GONE);
                myList.setVisibility(View.VISIBLE);
                clearArray();
                //массив для хранения названий типов продуктов
                String name_types[] = new String[Constant.MAX_SIZE_TYPE];
                 for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Food food = postSnapshot.getValue(Food.class);
                    // Log.d(MY_TAG, "Food type: " + food.getType());
                    //ищем в массиве есть ли такой продукт
                    have_type = false;
                    for (int i = 0; i < count_type; i++) {
                        if (food.getType().equals(name_types[i])) {
                            have_type = true;
                        }
                    }
                    //добавляем тип продукта в список
                    if (have_type == false) {
                        //Log.d(MY_TAG, "Food type: " + food.getType());
                        name_types[count_type]= food.getType();
                        count_type = count_type + 1;
                    }

                }
                //создаем массив под нужды сортировки
                String sort_ar[] = new String[count_type];
                 for(int i=0;i<count_type;i++){
                     sort_ar[i] = name_types[i];
                 }
                //сортируем массив
                Arrays.sort(sort_ar);


                //ищем тип продукта и дописываем ему название в массив
                for (int i = 0; i < count_type; i++) {
                    //Food tmp_food = ArrFood[i][0];
                    //Log.d(Constant.MY_TAG, "Type: " + tmp_food.getType() + " Name: " + tmp_food.getCity());
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Food food = postSnapshot.getValue(Food.class);
                        if (food.getType().equals(sort_ar[i])) {
                            //ищем окончание данных в массиве
                            ArrFood[i][getEnd(i)] = food;
                            Log.d(Constant.MY_TAG, "Type: " + food.getType() + "Name: " + food.getName());
                        }
                    }
                }
                //заполняем типы продуктов
                typeList.clear();
                ArrayList<Food> foodList;
                for (int i = 0; i < count_type; i++) {
                    foodList = new ArrayList<Food>();
                    foodList.clear();
                    for (int j = 0; j < getEnd(i); j++) {
                        foodList.add(ArrFood[i][j]);
                        ListForAutoCompleate.writeStringInArray(Constant.AR_TYPE,ArrFood[i][j].getType());
                        ListForAutoCompleate.writeStringInArray(Constant.AR_NAME,ArrFood[i][j].getName());
                        ListForAutoCompleate.writeStringInArray(Constant.AR_UNIT,ArrFood[i][j].getCurrency());
                        //такое впечатление что если строка длинная, то адрес принимается как уникальный, поэтому его надо обрезать
                        ListForAutoCompleate.writeStringInArray(Constant.AR_Located,cutAddress(ArrFood[i][j].getAddress()));
                    }
                    //добавляем тип
                    TypeFood type_food = new TypeFood(ArrFood[i][0].getType(), foodList);
                    //Log.d(Constant.MY_TAG, "Food type: " + ArrFood[i][0].getType());
                    typeList.add(type_food);
                }
                listAdapter = new FoodListAdapter(context, typeList);
                Log.d(Constant.MY_TAG, "List adapter " + listAdapter.toString());
                //пока отключил
                myList.setAdapter(listAdapter);
                //посылаем в главную активити данныу о том что считали все
                if(listAdapter.isEmpty()){
                    Log.d(Constant.MY_TAG, "No data in this town");
                    h.sendEmptyMessage(Constant.HANDLER_CITY_EMPTY);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(Constant.MY_TAG, "Failed to read value." + databaseError.toException().toString());
            }
        });
    }

    private int getEnd(int count){
        int count_name=0; //элемент массива в которой необходимо записать название, если весь массив заполнен то переписываем последний
        //Log.d(Constant.MY_TAG, "begin" + String.valueOf(count_name));
        for(int j=0;j<ArrFood[count].length;j++) {
            if(ArrFood[count][j]==null){
                count_name = j;//так как на один меньше
                break;
            }
        }
        return count_name;
    }


    //method to expand all groups
    public void expandAll() {
        int count = listAdapter.getGroupCount();
        for (int i = 0; i < count; i++){
            myList.expandGroup(i);
        }
    }

    //очистка массиа для записи новых данных
    private void clearArray(){
        for (int i = 0; i < ArrFood.length; i++) {
            //запрашиваем номер конца  массива продуктов
            if(ArrFood[i][0] == null) break;
            int numb = getEnd(i);
                for(int j=0;j<numb;j++){
                   ArrFood[i][j]=null;
                }
        }
        count_type = 0;
    }

    public void writeFoodToDB(Food _food){
       mDatabase.push().setValue(_food);
    }

    public static String cutAddress(String _origin){
        String result="";
        Log.d(Constant.MY_TAG,"Origin: "+ _origin);
        Log.d(Constant.MY_TAG,"City: "+ Cities.getCityName());
        if(_origin.contains(Cities.getCityName())){
            //ищем начало города встроке адреса
            int index = _origin.indexOf(Cities.getCityName());
            index = index - 2;//убираем пробел и запятую
            Log.d(Constant.MY_TAG,"Index: "+ String.valueOf(index));
            if(index != 0) {
                try {
                    result = _origin.substring(0, index);
                    Log.d(Constant.MY_TAG,"newResult: " + result);
                }catch(Exception e){
                    Log.d(Constant.MY_TAG,"Substring exception: " + e.getMessage());
                    return _origin;
                }
            }
        }else{
            return _origin;
        }
        return result;
    }

    public void deleteData(String nameData){
        //String uid = mDatabase.child(nameData).getKey();
        Log.d(Constant.MY_TAG,"Delete query: " + Constant.ORDER_QUERY + ", " + nameData);
        mDatabase.orderByChild(Constant.ORDER_QUERY).equalTo(nameData).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               for(DataSnapshot postsnapshot :dataSnapshot.getChildren()){
                   String key = postsnapshot.getKey();
                   Log.d(Constant.MY_TAG,"Key: " + String.valueOf(key));
                   mDatabase.child(key).removeValue();
               }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(Constant.MY_TAG,"Data is not detected");
            }
        });
        //Log.d(Constant.MY_TAG,"delete uid: " + uid);
        //mDatabase.child(uid).removeValue();
    }

    //обновить значение записи в БД
    public void updateData(Food mFood){
        deleteData(staticFood.getStaticFood().getName());
        writeFoodToDB(mFood);
    }

    //обновить пользователя в БД
    public void writeUserToDB(User _user){
        _user.setRecordState(true);
        mDatabase.push().setValue(_user);
    }

    public void readUserFromBD(final User _user,final Handler _h){
        //String uid = mDatabase.child(nameData).getKey();
        Log.d(Constant.MY_TAG,"setUser Search user: " + Constant.ORDER_EMAIL + ", " + _user.getEmail());
        mDatabase.orderByChild(Constant.ORDER_EMAIL).equalTo(_user.getEmail()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postsnapshot :dataSnapshot.getChildren()){
                    User m_user = postsnapshot.getValue(User.class);

                    User_static.setUser(m_user.getName(), m_user.getEmail(),m_user.getEditFood(),
                            m_user.getEditUser(), m_user.getRecord(),m_user.getEditCity());
                    _h.sendEmptyMessage(Constant.HANDLER_USER);
                    Log.d(Constant.MY_TAG,"read User: " + User_static.String());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(Constant.MY_TAG,"User is not detected");
                _h.sendEmptyMessage(Constant.HANDLER_USER_CANCEL);
            }
            /*
            private void setUser(User m_user){
                _user.setUser(m_user.getName(), m_user.getEmail(),m_user.getEditFood(),
                        m_user.getEditUser(), m_user.getRecord(),m_user.getEditCity());
                Log.d(Constant.MY_TAG, "m_user: " + m_user.toString());
                Log.d(Constant.MY_TAG, "_user: " + _user.toString());
            }*/
        });
        //Log.d(Constant.MY_TAG,"delete uid: " + uid);
        //mDatabase.child(uid).removeValue();
    }
}
