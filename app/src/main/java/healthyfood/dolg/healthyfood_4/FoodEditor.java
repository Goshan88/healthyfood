package healthyfood.dolg.healthyfood_4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import healthyfood.dolg.healthyfood_4.validators.BaseValidator;
import healthyfood.dolg.healthyfood_4.validators.EditTextValidator;
import healthyfood.dolg.healthyfood_4.validators.NotEmptyChecker;
import healthyfood.dolg.healthyfood_4.validators.ValidationSummary;
import healthyfood.dolg.healthyfood_4.validators.staticFood;

public class FoodEditor extends AppCompatActivity implements View.OnClickListener{

    AutoCompleteTextView eType,eName, eUnit, eLocated;
    EditText eCost, eNote;
    TextView tvError;
    ValidationSummary summaryValidator_;
    private healthyfood.dolg.healthyfood_4.DataBase DataBase;
    ImageButton SearchAddress;
    Cities City;
    private String edit_mode;
    private String user_name;
    private String user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_food_editor);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle(getString(R.string.activity_title_add_food));
        setSupportActionBar(mActionBarToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

         mActionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Log.d(Constant.MY_TAG, "Click on back arrow");
                finish();
            }
        });

        City = new Cities(this,FoodEditor.this);

        eType = (AutoCompleteTextView) findViewById(R.id.type_edit);
        eName = (AutoCompleteTextView) findViewById(R.id.name_edit);
        eCost = (EditText) findViewById(R.id.cost_edit);
        eUnit = (AutoCompleteTextView) findViewById(R.id.unit_edit);
        eLocated = (AutoCompleteTextView) findViewById(R.id.located_edit);
        eNote = (EditText) findViewById(R.id.note_edit);
        tvError = (TextView) findViewById(R.id.error_title);

        DataBase = new DataBase(this,City.getCity());

        initValidators();

        eType.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, ListForAutoCompleate.getType()));
        eName.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, ListForAutoCompleate.getName()));
        eUnit.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, ListForAutoCompleate.getUnit()));
        eLocated.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, ListForAutoCompleate.getAddress()));

        SearchAddress = (ImageButton) findViewById(R.id.search_address);
        SearchAddress.setOnClickListener(this);

        //запрашиваем данные из которые загрузились вместе с активити
        Intent intent = getIntent();
        edit_mode = intent.getStringExtra(Constant.EDITOR_MODE);
        if(edit_mode.equals(Constant.FOOD_EDIT)){
            eType.setText(staticFood.getStaticFood().getType());
            eName.setText(staticFood.getStaticFood().getName());
            eCost.setText(staticFood.getStaticFood().getCost());
            eUnit.setText(staticFood.getStaticFood().getCurrency());
            eLocated.setText(staticFood.getStaticFood().getAddress());
            eNote.setText(staticFood.getStaticFood().getInfo());
        } else if(edit_mode.equals(Constant.FOOD_EDIT)) {

        }
        user_name = intent.getStringExtra(Constant.EDITOR_NAME);
        user_email = intent.getStringExtra(Constant.EDITOR_EMAIL);
    }

    @Override
    public void onClick(View v){
        Intent intent = new Intent(this, SearchPlace.class);
        intent.putExtra(Constant.PLACE_MODE,Constant.SEARCH_MARKET);
        startActivityForResult(intent,Constant.SEARCH_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if(data == null){return;}
        String address = data.getStringExtra(Constant.RESULT_ADDRESS);
        Log.d(Constant.MY_TAG,"Address were choose: "+address);
        eLocated.setText(healthyfood.dolg.healthyfood_4.DataBase.cutAddress(address));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    /*    MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
    */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_enter){
            summaryValidator_.performCheck();
            if(summaryValidator_.isCorrect()) {
                if(edit_mode.equals(Constant.FOOD_EDIT)) {
                    Food newFood = new Food(eType.getText().toString(), eName.getText().toString(),
                            eCost.getText().toString(), eUnit.getText().toString(),
                            eLocated.getText().toString(), user_name, eNote.getText().toString());
                    DataBase.updateData(newFood);
                }else{
                    Food newFood = new Food(eType.getText().toString(), eName.getText().toString(),
                            eCost.getText().toString(), eUnit.getText().toString(),
                            eLocated.getText().toString(), user_name, eNote.getText().toString());
                    DataBase.writeFoodToDB(newFood);
                }
                    finish();
            }else{
                tvError.setText(getString(R.string.empty_field));
                tvError.setVisibility(View.VISIBLE);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initValidators(){
        //Создали группирующий валидатор
        summaryValidator_ = new ValidationSummary(getString(R.string.type_error_empty), tvError);

        createValidator(summaryValidator_,eType,getString(R.string.type_error_empty));
        createValidator(summaryValidator_,eName,getString(R.string.name_error_empty));
        createValidator(summaryValidator_,eCost,getString(R.string.cost_error_empty));
        createValidator(summaryValidator_,eUnit,getString(R.string.unit_error_empty));
        createValidator(summaryValidator_,eLocated,getString(R.string.located_error_empty));
    }

    private void createValidator(ValidationSummary _summaryValidator, EditText _EditText, String sEmptyError){
        //Создали валидатор для поля ввода имени
        EditTextValidator nameValidator = new EditTextValidator();
        //Назначили ему EditText для валидации, выбрав ручной режим проверки (не будет валидации, когда пользователь начнет вводить текст)
        nameValidator.setViewToValidate(_EditText, BaseValidator.ValidationMode.Auto);
        //Установили TextView для отображения сообщений об ошибках в нем, а не в штатном всплывающем окне
        nameValidator.setExternalErrorView( (TextView) findViewById(R.id.error_title));
        //Добавили проверки на пустоту строки
        nameValidator.addConditionChecker(new NotEmptyChecker(sEmptyError));
        //и ее длину
        //nameValidator.addConditionChecker(new LengthChecker(Constant.MIN_SIZE_VALIDATORS, Constant.MAX_SIZE_VALIDATORS, getString(R.string.lenth_error_message) ));
        //Добавили валидатор в группу
        _summaryValidator.addValidator(nameValidator);
    }
}
